Wifi captive AP is a project build for educational purposes.

It creates a wifi AP that tricks users to login with their Google, Facebook, Twitter or Linkedin account, showing how feasible is to steal user login data.

It is derived from [RougueWifi](https://github.com/s0meguy1/RougeWifi)


**This project is build for educational purposes. Authors are not responsible of the damages that the malicious use of this project could cause.**