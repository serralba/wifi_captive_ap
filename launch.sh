echo "Configuring wlan0"
nmcli radio wifi off
rfkill unblock wlan
ifconfig wlan0 up 10.0.0.1 netmask 255.255.255.0
sleep 1

echo "Configuring iptables"
################ Redirection of not authenticated hosts
iptables -t mangle -N captiveportal
iptables -t mangle -A PREROUTING -i wlan0 -p udp --dport 53 -j RETURN  ## dns do not treated
iptables -t mangle -A PREROUTING -i wlan0 -j captiveportal  ## the rest of traffic to captive portal
iptables -t mangle -A captiveportal -j MARK --set-mark 1  # Mark not authenticated hosts
iptables -t nat -A PREROUTING -i wlan0 -p tcp --dport 80 -m mark --mark 1 -j DNAT --to-destination 10.0.0.1:80 ## Redirect to captive portal
iptables -t nat -A PREROUTING -i wlan0 -p tcp --dport 443 -m mark --mark 1 -j DNAT --to-destination 145.14.144.89:443  # Redirections to HTTPS captive portal

## This rule is added to skip authentication in captive portal
#iptables -I captiveportal 1 -t mangle -m mac --mac-source $mac -j RETURN

################ Forward the rest of traffic (authenticated hosts)
iptables -A FORWARD -i wlan0 -j ACCEPT  # Accept forwarded connections from wlan0 
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE  # Nating for output

sysctl -w net.ipv4.ip_forward=1

echo "Restarting services"
service apache2 restart
service dnsmasq restart
service hostapd restart
sleep 2
