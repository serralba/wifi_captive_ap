#!/bin/bash



echo "Stoping conflictive services"
systemctl disable systemd-resolved

echo "Installing packages"
apt install -y hostapd dnsmasq apache2 php libapache2-mod-php

sleep 5

echo "Configuring hostapd"
if [ -f /etc/hostapd/hostapd.conf ]; then mv /etc/hostapd/hostapd.conf /etc/hostapd/hostapd.BAK; fi
cp ./hostapd_egokitu.conf /etc/hostapd/hostapd.conf

echo "Configuring dnsmasq"
if [ -f /etc/dnsmasq.conf ]; then mv /etc/dnsmasq.conf /etc/dnsmasq.BAK; fi
cp ./dnsmasq.conf /etc/dnsmasq.conf

echo "Configuring CaptivePortal in apache2"
a2dismod mpm_event
a2enmod ssl php*
echo "<IfModule mod_ssl.c>
        <VirtualHost _default_:443>
                RedirectMatch permanent ^ http://10.0.0.1:81/login.html

                ServerAdmin webmaster@localhost

                DocumentRoot /var/www/html

                ErrorLog ${APACHE_LOG_DIR}/error.log
                CustomLog ${APACHE_LOG_DIR}/access.log combined

                SSLEngine on

                SSLCertificateFile      /etc/ssl/certs/apache-selfsigned.crt
                SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key

                <FilesMatch \"\.(cgi|shtml|phtml|php)$\">
                                SSLOptions +StdEnvVars
                </FilesMatch>
                <Directory /usr/lib/cgi-bin>
                                SSLOptions +StdEnvVars
                </Directory>

        </VirtualHost>
</IfModule>
" > /etc/apache2/sites-enabled/default-ssl.conf

cp 000-default.conf /etc/apache2/sites-enabled/000-default.conf

echo "SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH
SSLProtocol All -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
SSLHonorCipherOrder On
Header always set X-Frame-Options DENY
Header always set X-Content-Type-Options nosniff
# Requires Apache >= 2.4
SSLCompression off
SSLUseStapling on
SSLStaplingCache \"shmcb:logs/stapling-cache(150000)\"
# Requires Apache >= 2.4.11
SSLSessionTickets Off" > /etc/apache2/conf-available/ssl-params.conf

cp *.crt /etc/ssl/certs/
cp *.key /etc/ssl/private/
cp -r ./web_files/* /var/www/html
chown www-data:www-data /var/www/html
echo "www-data ALL=(ALL:ALL) NOPASSWD: /sbin/iptables, /usr/sbin/arp" >> /etc/sudoers

echo "Launching..."
bash launch.sh

